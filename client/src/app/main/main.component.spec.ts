import { TestBed, async, getTestBed, tick, fakeAsync } from '@angular/core/testing';
import { MainComponent } from './main.component';
import { ChangeResultsComponent } from '../change-results/change-results.component';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('MainComponent', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientTestingModule],
      declarations: [
        ChangeResultsComponent,
        MainComponent
      ],
    }).compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(MainComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));

  it(`should have initial empty state`, async(() => {
    const fixture = TestBed.createComponent(MainComponent);
    const comnponent = fixture.debugElement.componentInstance as MainComponent;
    expect(comnponent.amount).toBe(undefined);
    expect(comnponent.amountRequested).toBe(undefined);
    expect(comnponent.changeResults).toBe(undefined);
    expect(comnponent.denominations).toEqual([100, 50, 20, 10]);
    expect(comnponent.errorMessage).toBe(undefined);
    expect(comnponent.requestPending).toBe(false);
  }));

  it('should render results after they come', fakeAsync(() => {
    const fixture = TestBed.createComponent(MainComponent);
    const component = fixture.debugElement.componentInstance as MainComponent;
    fixture.detectChanges();

    component.amount = 120;
    fixture.detectChanges();
    component.requestChange();

    const req = httpMock.expectOne('./change/120');
    expect(req.request.method).toBe('GET');
    req.flush({ change: [100, 20] });
    fixture.detectChanges();
    tick();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('.denomination-result').length).toBe(2);
    expect(compiled.querySelector('.error-message')).toBe(null);
  }));
});
