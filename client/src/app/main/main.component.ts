import { Component } from '@angular/core';
import { Denomination, ChangeResults } from '../models/models';
import { ChangeService } from '../services/change.service';

@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {

  readonly denominations = [100, 50, 20, 10] as Denomination[];

  changeResults: ChangeResults;
  amount: number;
  amountRequested: number;
  requestPending = false;
  errorMessage: string;

  constructor(private change: ChangeService) { }

  /**
   * Requests the change count for a given amount.
   */
  requestChange() {
    this.resetState();
    this.amountRequested = this.amount;
    this.change.getChangeForAmount(this.amount).then(result => {
      this.changeResults = result;
      this.requestPending = false;
    }, error => {
      this.errorMessage = error.message;
      this.requestPending = false;
    });
  }

  /**
   * Resets internal properties on the component.
   */
  private resetState() {
    this.errorMessage = null;
    this.requestPending = true;
    this.changeResults = null;
    this.amountRequested = null;
  }
}
