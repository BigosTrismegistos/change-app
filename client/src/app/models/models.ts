
export type Denomination = number;

export interface ChangeResults {
  [denomination: number]: number;
}
