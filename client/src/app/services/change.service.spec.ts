import { TestBed, inject, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ChangeService } from './change.service';

describe('ChangeService', () => {
  let injector: TestBed;
  let service: ChangeService;
  let httpMock: HttpTestingController;

  beforeEach((done) => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ChangeService]
    });
    injector = getTestBed();
    service = injector.get(ChangeService);
    httpMock = injector.get(HttpTestingController);
    done();
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send request to a correct url', async () => {
    service.getChangeForAmount(120).then();

    const req = httpMock.expectOne('./change/120');
    expect(req.request.method).toBe('GET');
    req.flush({ change: [100, 20] });

  });

  it('should return results in correct format', () => {
    service.getChangeForAmount(120).then(results => {
      expect(results[100]).toEqual(1);
      expect(results[20]).toEqual(1);
    });

    const req = httpMock.expectOne('./change/120');
    expect(req.request.method).toBe('GET');
    req.flush({ change: [100, 20] });
  });
});
