import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ChangeResults } from '../models/models';

@Injectable({
  providedIn: 'root'
})
export class ChangeService {

  constructor(private http: HttpClient) { }

  /**
   * Requests the change from the backend for a given amount.
   * @param {number} amount The amount to make change for.
   */
  public async getChangeForAmount(amount: number): Promise<ChangeResults>  {
    return this.http.get(`./change/${amount}`).toPromise().then((response: any) => {
      try {
        if (response.change instanceof Array) {
          return this.groupByDenomination(response.change);
        } else {
          throw new Error(`Couldn't parse response`);
        }
      } catch (e) {
        throw new Error(`Couldn't parse response`);
      }
    }, response => {
      throw new Error(`Couldn't fetch data: ${response.statusText}, ${response.error}`);
    });
  }

  private groupByDenomination(rawResults: number[]): ChangeResults {
    const result = { };
    rawResults.forEach((banknote) => {
      if (result[banknote] == null) {
        result[banknote] = 1;
      } else {
        result[banknote]++;
      }
    });
    return result;
  }
}
