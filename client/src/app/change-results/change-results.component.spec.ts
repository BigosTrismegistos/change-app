import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeResultsComponent } from './change-results.component';

describe('ChangeResultsComponent', () => {
  let component: ChangeResultsComponent;
  let fixture: ComponentFixture<ChangeResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
