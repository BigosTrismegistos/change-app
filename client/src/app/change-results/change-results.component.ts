import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-change-results',
  templateUrl: './change-results.component.html',
  styleUrls: ['./change-results.component.css']
})
export class ChangeResultsComponent {

  @Input() amount: number;
  @Input() denominations: number[];
  @Input() changeResults: { [denomination: number]: number };

}
