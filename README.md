# Change making app


This a solution to a code challenge by Arkadiusz Gochnio.

The challenge was to build an app that, for a given amount, would find a least number of banknotes in which this amount could be expressed.

## Installing and running the app

Pre-requisites: the app assumes that node is installed.

1. Run `npm install` in repo's main directory. This will install both server and client dependencies.
2. Run `npm run start`. This will build the client application and start the app server.
3. Access the app by opening `http://localhost:4200/` with a web browser

To run app's tests run `npm test`. This will run both client and server tests.

## App structure

The app actually consists of two separate apps. Backend and frontend part.

The directory structure is as follows:

* `client/` directory contains the frontend app
* `server/` directory contains the backend app
* `spec` directory contains server side tests
* `public/` directory contains build artifacts after the frontend apps has been built

### Backend

The server part is a simple NodeJS app written with ExpressJS framework. It's main purpose is to encapsulate the business logic and to expose an endpoint to the frontend app.

### Frontend

The frontend app is written with Angular 6. It communicates with the server over REST API.