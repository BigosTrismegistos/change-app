const computeChange = require('../server/services/change.service');

describe('Change-making problem tests', () => {
  it('Correctly makes change of 30', () => {
    expect(computeChange(30).sort()).toEqual([20, 10].sort());
  });

  it('Correctly makes change of 80', () => {
    expect(computeChange(80).sort()).toEqual([50, 20, 10].sort());
  });

  it('Correctly makes change of 0', () => {
    expect(computeChange(0)).toEqual([]);
  })
});

describe('Invalid argument tests: ', () => {

  it('Reacts correctly to null', () => {
    expect(computeChange(null)).toEqual([]);
  })

  it('Throws an error when argument is of wrong type', () => {
    expect(() => { computeChange('a string') }).toThrow(
      new Error('InvalidArgument: The amount passed is not a number.')
      );
  });

  it('Throws an error when unable to make the change', () => {
    expect(() => { computeChange(125) }).toThrow(
      new Error('MissingNote: The amount specified could not be expressed using notes: 100, 50, 20, 10')
      );
  });

  it('Throws an exception when argument is wrong', () => {
    expect(() => { computeChange(-125) }).toThrow(
      new Error('InvalidArgument: The amount passed is negative.')
    );
  });
});