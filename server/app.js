const express = require('express');
const app = express();

const changeRoute = require('./routes/change');
const port = 4200;

app.get('/change/:amount', changeRoute);
app.use(express.static('public'));
app.listen(port);

module.exports = app;