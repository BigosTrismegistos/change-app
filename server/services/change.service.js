

const denominations = [100, 50, 20, 10]

/**
 * Computes the least amount of banknotes that could be used to express the `amount`.
 * It uses a dynamic programming algorithm that computes optimal results
 * @param {number} amount The amount of money to be expressed by the banknotes.
 */
function computeChange(amount) {
  if (amount == null) {
    return [];
  }
  validateAmount(amount);
  const results = initalizeResultArray(amount);
  const usedDenomination = [];
  for (let denomination of denominations) {
    //console.log('Counting denomination: ', denomination);
    for (let partialAmount = 0; partialAmount < (amount - denomination) + 1; partialAmount++) {
      //console.log(`Counting partial amount: ${partialAmount}, result: ${results[partialAmount]}`);
      if (results[partialAmount] < Infinity) {
        if (results[partialAmount] + 1 < results[partialAmount + denomination]) {
          results[partialAmount + denomination] = results[partialAmount] + 1;
          usedDenomination[partialAmount + denomination] = denomination;
          //console.log(`Found better way for $${denomination + partialAmount}, it's ${reconstructDenominationSequence(usedDenomination, partialAmount+denomination)}`);
        }
      }
    } 
  }
  return reconstructDenominationSequence(usedDenomination, amount);
}

/**
 * Prepares the result array for given amount. After the algorithm is ran, the final result will be placed in
 * last cell of the array.
 * @param {number} amount The amount for which the array has to be prepared.
 */
function initalizeResultArray(amount) {
  const result = [0]; // T[0] = 0;
  // All other cells up to `amount` are `Infinity` at the start.
  for(let i = 1; i < amount + 1; i++) {
    result[i] = Infinity;
  }
  return result;
}

function reconstructDenominationSequence(results, amount) {
  let amountLeft = amount;
  const denominationSequence = [];
  while (amountLeft > 0) {
    const usedBanknote = results[amountLeft];
    amountLeft = amountLeft - usedBanknote;
    denominationSequence.push(usedBanknote);
  }
  if (amountLeft !== 0) {
    throw new Error('MissingNote: The amount specified could not be expressed using notes: 100, 50, 20, 10');
  }
  return denominationSequence;
}

function validateAmount(amount) {
  if (typeof amount !== 'number') {
    throw new Error('InvalidArgument: The amount passed is not a number.');
  }
  if (amount < 0) {
    throw new Error('InvalidArgument: The amount passed is negative.');
  }
}

module.exports = computeChange;