var express = require('express');
var router = express.Router();
var computeChange = require('../services/change.service');

router.get('/change/:amount', (req, res) => {
  if(!req.params || req.params.amount == null) {
    res.status(400).send('Wrong or no amount');
  } else {
    const parsedAmount = Number.parseInt(req.params.amount);
    if (typeof parsedAmount == 'number' && !isNaN(parsedAmount)) {
      try { 
        const computedChange = computeChange(parsedAmount);
        res.send({ change: computedChange} );
      } catch (e) {
        res.status(400).send(`Not possible to pay out: ${e.message}`);
      }
    } else {
      res.status(400).send('Unrecognized amount');
    }
  }
});

module.exports = router;